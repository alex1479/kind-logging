# Fluent Bit logging in k8s

## pre

- brew install kind
- kind create cluster --config config.yaml
- k config set-context kind-kind
- k apply -f counter-pod.yaml
- helm repo add fluent https://fluent.github.io/helm-charts
- helm upgrade --install fluent-bit fluent/fluent-bit -f fb-values.yaml

## links
- https://yuki-nakamura.com/2023/10/15/get-kubelets-metrics-manually/